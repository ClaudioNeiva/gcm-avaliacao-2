package br.ucsal.bes20211.gcm.avaliacao2;

import java.io.ByteArrayInputStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculoInTest {

	@Test
	void testarSoma34_12() throws InterruptedException {

		Thread.sleep(60000);

		ByteArrayInputStream inFake = new ByteArrayInputStream("34\n12".getBytes());
		System.setIn(inFake);

		Calculo calculo = new Calculo();

		int somaEsperada = 46;

		// como faço pra passar n1 e n2???
		int somaAtual = calculo.calcularSoma();

		Assertions.assertEquals(somaEsperada, somaAtual);
	}

}
